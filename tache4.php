<?php
include('./template/header.php'); 
?>

<main>
<?php
    $data = [0, 100, 40, 50];
    echo "<h5> Démo du parcours tab simple </h5>";
    foreach($data as $key => $value) {
        echo $key . " : ".$value . "<br />";
    }

    $data = [ "prenom" => "Antony", "age" => 36];
    echo "<h5> Démo du parcours tab assoc </h5>";
    foreach ($data as $key => $value) {
        echo $key . " : ".$value . "<br />";
    }
    
    $data = [ 
        [ "prenom" => "Antony", "age" => 36],
        [ "prenom" => "Michel", "age" => 66]
    ];

    echo "<h5> Démo du parcours d'un tab contenant tab assoc </h5>";
    foreach ($data as $user) {
        echo $user['prenom'] . " a l'âge de " . $user['age'] . " ans. <br/> ";
    }

    echo "<h5> J'affiche toutes les variables de l'URL : </h5>";
    foreach ($_SERVER as $key => $value) {
        echo $key . " : ".$value . "<br />";
    }

?>
</main>


<?php include('./template/footer.php');  ?>
