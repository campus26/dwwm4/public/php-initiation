<?php
session_start();
include('./utils/functions.php');
include('./template/header.php'); 

//Permet de réinit le 
resetGame();
playGame();
?>

<main>
    <?php displayBoard(); ?>
    <?php displayWin(); ?>
</main>


<?php include('./template/footer.php');  ?>
