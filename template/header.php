<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sommaire de mon site</title>
    <link rel="stylesheet" href="./assets/css/style.css" />
</head>
<body>
    <header>
    <ul>
        <li><a href="/">HOME</a></li>
        <li><a href="/tache1.php">Exercice 1</a></li>
        <li><a href="/tache2.php">Exercice 2</a></li>
        <li><a href="/tache3.php">Exercice 3</a></li>
        <li><a href="/tache4.php">Exercice 4</a></li>
        <li><a href="/game.php?reset">Game</a></li>
    </ul>
</header>