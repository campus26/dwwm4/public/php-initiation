<?php 
    require('./utils/functions.php');
    include('./template/header.php'); 

    $kit = 10;
    $kat = 20;
    $roger = 100 + add($kit, $kat);
?>
<main>
    <p>Je vous présente deux variables <b>kit</b> valant <?php echo $kit; ?> et <b>kat</b> valant <?php echo $kat; ?></p>
    <p>Quand on <b>additionne</b> kit et kat on obtient : <?php echo $roger; ?></p>
    <p>Quand on <b>soustrait</b> kit à kat on obtient : <?php echo $kat - $kit; ?></p>
    <p>Quand on <b>multiplie</b> kit avec kat on obtient : <?php echo $kit * $kat; ?></p>
</main>

<?php include('./template/footer.php');  ?>
