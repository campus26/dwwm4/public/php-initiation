<?php

/*************/
/* Permet de réinit la totalité des variables du P4 */
/*************/
function resetGame(){
    if(isset($_GET['reset'])){
        session_unset();
    }
}

/*************/
/* Permet de récupérer le plateau de jeu P4 */
/*************/
function getBoard()
{
    $x = 6; $y = 7;
    if(!isset($_SESSION['game'])){
        $data=[];
        for($i=0; $i < $x; $i++){
            for($j=0; $j < $y; $j++){
                $data[$i][$j] = "";
            }
        }
        return $data;
    }else
        $data = $_SESSION['game'];
    return $data;
}

function setBoard($data){
    $_SESSION['game'] = $data;
}


/*************/
/* Permet de check le joueur en cours et d'alterner le cas échéant */
/*************/
function getPlayer(){
    if(!isset($_SESSION['player'])){
        $player = "ja";
        $_SESSION['player'] = $player;
    }else{
        $player = $_SESSION['player'];
        ($player == "ja") ? $player = "jb" : $player = "ja";
        $_SESSION['player'] = $player;
    }
    return $player;  
}

function playGame(){
    $data = getBoard();
    $player = getPlayer();
    if(isset($_GET['colonne'])){
        for($i = 0; $i< sizeof($data); $i++){
            if($data[$i][$_GET['colonne']] == ""){
                $data[$i][$_GET['colonne']]= $player;
                break;
            }
    
        }
        setBoard($data);
    }
}

function check(){
    $data = getBoard();
    if(checkhorizontale()) return  checkhorizontale();
    if(checkverticale()) return  checkverticale();
    if(checkdiagonale()) return  checkdiagonale();

}

function checkhorizontale()
{
    $data = getBoard();
    $cpt = 0;
    for($i = 0; $i < sizeof($data); $i++ ){
        for ($j = 0; $j < sizeof($data[$i]) - 1; $j++) {
            $cpt++;
            if ( ($data[$i][$j] == $data[$i][$j + 1]) && (!empty($data[$i][$j])) ) {
            } else {
                $cpt = 0;
            }
            if( $cpt == 3 ) return $data[$i][$j]; 
        }
    }
    return false;
    die;
}

function checkverticale()
{
    $data = getBoard();
    $cpt = 0;
    for($j = 0; $j < sizeof($data[0]); $j++ ){
        for ($i = 0; $i < sizeof($data) - 1; $i++) {
            $cpt++;
             if ( ($data[$i][$j] == $data[$i + 1][$j]) && (!empty($data[$i][$j])) ) {
            } else{
                 $cpt = 0;
             }
            if( $cpt == 3 ) return $data[$i][$j]; 
        }
    }
    return false;
    die;
}

// Fonction pour vérifier les diagonales du jeu Puissance 4
function checkdiagonale() {

    $jeu = getBoard();
    $hauteur = sizeof($jeu);
    $largeur = sizeof($jeu[0]);

    // Vérification des diagonales de gauche à droite (de haut en bas)
    for ($i = 0; $i < $hauteur - 3; $i++) {
        for ($j = 0; $j < $largeur - 3; $j++) {
            $pion = $jeu[$i][$j];
            if ($pion !== "" &&
                $pion === $jeu[$i + 1][$j + 1] &&
                $pion === $jeu[$i + 2][$j + 2] &&
                $pion === $jeu[$i + 3][$j + 3]) {
                return $pion;
            }
        }
    }

    // Vérification des diagonales de droite à gauche (de haut en bas)
    for ($i = 0; $i < $hauteur - 3; $i++) {
        for ($j = 3; $j < $largeur; $j++) {
            $pion = $jeu[$i][$j];
            if ($pion !== "" &&
                $pion === $jeu[$i + 1][$j - 1] &&
                $pion === $jeu[$i + 2][$j - 2] &&
                $pion === $jeu[$i + 3][$j - 3]) {
                return $pion;
            }
        }
    }

    // Aucun gagnant sur les diagonales
    return false;
}


function displayBoard()
{
    $data = getBoard();
    echo '<div class="p4">';

    echo '<section>';
    foreach($data[0] as $key => $value) {
        echo'<div><a href="/game.php?colonne=' . $key . '"></a></div>';
    }
    echo "</section>";

    foreach ($data as $row) {
        echo '<section>';
        foreach ($row as $cell) {
            echo '<div class="' . $cell . ' "> </div>';
        }
        echo "</section>";
    }
    
    echo '</div>';

    function displayWin(){
        $winner = check();
        if($winner){
            echo '<section class="win '.$winner.'">';
                    switch($winner){
                        case "ja": 
                            echo "<p>Le Joueur A a gagné</p>";
                        break;
                        case "jb": 
                            echo "<p>Le Joueur B a gagné</p>";
                        break;
                    }
            echo '<a href="game.php?reset">REJOUER</a>';
            echo '</section>';
        }
    }
?>

<?php
}
